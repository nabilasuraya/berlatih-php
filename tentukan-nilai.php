<?php
function tentukan_nilai($number)
{
    //  kode disini
    if ( $number >= 90){
        echo "$number : Sangat Baik <br>";
    }elseif ($number >= 70 && $number <= 89) {
        echo "$number : Baik <br>";
    }elseif ($number >= 60 && $number <= 69) {
        echo "$number : Cukup <br>";
    }else{
        echo "$number : Kurang <br>";
    }
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
?>